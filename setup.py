from setuptools import setup, find_packages
import versioneer

ver = versioneer.get_version()

classifiers = ['Development Status :: 2 - Pre-Alpha',
               'Operating System :: POSIX :: Linux',
               'License :: OSI Approved :: Python Software Foundation License',
               'License :: OSI Approved :: GNU General Public License (GPL)',
               'Intended Audience :: Developers',
               'Programming Language :: Python :: 3',
               'Topic :: Software Development',
               'Topic :: System :: Hardware']

setup(name = 'eddyflux',
      version = ver,
      description = 'Python package to Eddy Flux processing.',
      license = 'GPLv3',
      classifiers = classifiers,
      url = 'https://gitlab.com/sehnem/eddyflux/',
      download_url = 'https://gitlab.com/sehnem/eddyflux/-/archive/{}/eddyflux-{}.tar.gz'.format(ver, ver),
      dependency_links = [],
      install_requires = ['pandas', 'numpy'],
      packages = find_packages(),
      include_package_data=True,
      cmdclass=versioneer.get_cmdclass(),
      zip_safe=False)
